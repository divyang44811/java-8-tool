package xesj.tool;
import java.io.Serializable;
/**
 * Minta Init osztály.
 * Javaslat: az osztály lemásolása, és refaktorálása Init névre.
 */
public class InitSample implements Serializable {
  /**
   * Singleton példány.
   * MÓDOSÍTÁSA TILOS !
   */
  private static volatile InitSample instance;
  /** 
   * Környezetfüggő változók (private, nem static).
   */
  private String kornyezet;
  private int random;
  /**
   * Konstruktor
   * PRIVATE !
   */
  private InitSample() {
    kornyezet = System.getProperty("kornyezet");
    if (kornyezet == null) throw new RuntimeException("A 'kornyezet' system property nincs beállítva");
    switch (kornyezet) {
      case "dev":
        random = (int)(Math.random()*1000000);
        break;
      case "real":
        random = (int)(-Math.random()*1000000);
        break;
      default:
        throw new RuntimeException("Ismeretlen környezet: '" + kornyezet + "'");
    }
  }
  /**
   * Init példány lekérdezés.
   * MÓDOSÍTÁSA TILOS !
   */
  public static InitSample getInstance() {
    if (instance == null) {
      synchronized (InitSample.class) {
        if (instance == null) {
          instance = new InitSample();
        }
      }
    }
    return instance;
  }
  /**
   * Újrainicializálás.
   * MÓDOSÍTÁSA TILOS !
   */
  public static synchronized void reInit() {
    instance = new InitSample();
  }
  /**
   * Getter metódusok.
   */
  public String getKornyezet() {
    return kornyezet;
  }
  public int getRandom() {
    return random;
  }
}
