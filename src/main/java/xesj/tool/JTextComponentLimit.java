package xesj.tool;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
/**
 * JTextField és JTextArea bevihető karaktereinek darabszám korlátozása. Használat:<br>
 * <code>jtextfield.setDocument(new JTextComponentLimit(100));</code>
 */
public class JTextComponentLimit extends PlainDocument {
  /**
   * Bevihető karakterek számának maximuma.
   */
  private int limit;
  /**
   * Konstruktor
   * @param limit Bevihető karakterek számának maximuma.
   */
  public JTextComponentLimit(int limit) {
    super();
    this.limit = limit;
  }
  /**
   * insertString()
   */
  @Override
  public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
    if (str == null) return;
    if (getLength() + str.length() <= limit) super.insertString(offset, str, attr);
  }
  // =====
}