package xesj.tool;
public class ByteTool {
  /**
   * Byte konvertálása 2 karakteres nagybetűs hexadecimális értékre pl: "A9"
   * @param bajt Konvertálandó byte.
   * @return Konvertált érték, például: "00", "F9", "3D". Null paraméter esetén null-t ad vissza.
   */
  public static String byteToHex(Byte bajt) {
    if (bajt == null) return null;
    return String.format("%02X", bajt);
  }
  /**
   * 1-2 karakteres hexadecimális string konvertálása byte-ra.
   * @param hex 1-2 karakteres hexadecimális string pl: "5", "d", "00", "f9", "F9", "3D", "3d". 
   * @throws NumberFormatException Ha a paraméter nem 1-2 karakter hosszú, vagy hibás hexadecimális string. 
   * @return Byte-ra konvertált érték. Null paraméter esetén null-t ad vissza.
   */
  public static Byte hexToByte(String hex) throws NumberFormatException {
    if (hex == null) return null;
    if (hex.length() < 1 || hex.length() > 2) {
      throw new NumberFormatException("A 'hex' paraméter csak 1 vagy 2 karakterből állhat");
    }
    return (byte)Integer.parseInt(hex, 16);
  }
  /**
   * Byte konvertálása 8 karakteres bináris értékre pl: "10110111"
   * @param bajt Konvertálandó byte.
   * @return Konvertált érték, például: "00000000", "10101010". Null paraméter esetén null-t ad vissza.
   */
  public static String byteToBin(Byte bajt) {
    if (bajt == null) return null;
    return StringTool.leftPad(Integer.toBinaryString((int)bajt & 0xFF), 8, '0');
  }
  /**
   * 1-8 karakteres bináris string konvertálása byte-ra.
   * @param bin 1-8 karakteres bináris string string pl: "11111111", "0110", "0". 
   * @throws NumberFormatException Ha a paraméter nem 1-8 karakter hosszú, vagy hibás bináris string. 
   * @return Byte-ra konvertált érték. Null paraméter esetén null-t ad vissza.
   */
  public static Byte binToByte(String bin) throws NumberFormatException {
    if (bin == null) return null;
    if (bin.length() < 1 || bin.length() > 8) {
      throw new NumberFormatException("A 'bin' paraméter csak 1-8 karakterből állhat");
    }
    return (byte)Integer.parseInt(bin, 2);
  }
  // =====
}
