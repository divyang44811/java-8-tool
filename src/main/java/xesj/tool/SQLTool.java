package xesj.tool;

/**
 * SQL-parancs összeállítását segítő tool
 */
public class SQLTool {
  
  private StringBuilder builder = new StringBuilder();

  /**
   * Konstruktor. 
   * Az SQL-parancs kezdőértéke üres string lesz. 
   */
  public SQLTool() {
  }

  /**
   * Konstruktor.
   * Az SQL-parancs kezdőértéke "str" lesz. Ha az "str" null, akkor az SQL-parancs kezdőértéke üres string lesz.
   * @param str Az SQL-parancshoz kezdőértéke. 
   */
  public SQLTool(String str) {
    if (str != null) {
      builder.append(str);
    }  
  }

  /**
   * Az "str" hozzáadása az SQL-parancshoz.
   * Ha az "str" null, akkor nem adódik hozzá semmi az SQL-parancshoz. 
   * @param str Az SQL-parancshoz hozzáadandó szöveg.
   */
  public SQLTool add(String str) {
    if (str != null) {
      builder.append(str);
    }  
    return this;
  }

  /**
   * INN (if not null).
   * Ha a "condition" nem null, akkor az "str"-t hozzáadja az SQL-parancshoz.
   * Ha az "str" null, akkor nem változik az SQL-parancs. 
   * @param str Az SQL-parancshoz hozzáadandó szöveg.
   * @param condition Feltétel.
   */
  public SQLTool inn(String str, Object condition) {
    if (condition != null && str != null) {
      builder.append(str);
    }  
    return this;
  }

  /**
   * ITR (if true).
   * Ha a "condition" igaz, akkor az "str"-t hozzáadja az SQL-parancshoz.
   * Ha az "str" null, akkor nem változik az SQL-parancs. 
   * @param str Az SQL-parancshoz hozzáadandó szöveg.
   * @param condition Feltétel.
   */
  public SQLTool itr(String str, boolean condition) {
    if (condition && str != null) {
      builder.append(str);
    }  
    return this;
  }
  
  /**
   * A végleges SQL-parancs string-ként.
   */
  @Override
  public String toString() {
    return builder.toString();
  }
 
  // =====
}
