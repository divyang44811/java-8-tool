package xesj.tool;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Core {

  /**
   * A program verziója.
   */
  public static String version() {
    Properties properties = new Properties();
    InputStream is = null;
    try {
      is = Core.class.getResourceAsStream("project.xml");
      properties.loadFromXML(is);
      return properties.getProperty("project.version");
    }
    catch (IOException e) {
      throw new RuntimeException(e);
    }
    finally {
      try { is.close(); } catch (Exception e) {}
    }
  }

  // =====
}
