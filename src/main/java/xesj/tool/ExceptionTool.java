package xesj.tool;
import java.io.PrintWriter;
import java.io.StringWriter;
public class ExceptionTool {
  /**
   * Stack trace string-re alakítása
   */
  public static String traceString(Throwable throwable) {
    StringWriter stringWriter = new StringWriter();
    throwable.printStackTrace(new PrintWriter(stringWriter));
    return stringWriter.toString();
  }  
  /**
   * Visszaadja az exception-lánc legelső elemét, vagyis amelyik legelőszőr keletkezett.
   * @param baseException Az exception-lánc egy eleme, mely alapján a legelső elem megkereshető.
   * @return Az exception-lánc legelső eleme. Ha a paraméter null, akkor null-t ad vissza.
   */
  public static Throwable getRootCause(Throwable baseException) {
    if (baseException == null) {
      return null;
    }
    Throwable rootException = baseException;
    while (rootException.getCause() != null) {
      rootException = rootException.getCause();
    }
    return rootException;
  }
  // =====
}
