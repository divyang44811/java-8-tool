package xesj.xml;
import java.io.File;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
public class XmlTool {
  /**
   * A szövegen xml escape-et hajt végre. 
   */
  public static String escape(String str) {
    if (str == null) return null;
    return str.replaceAll("&", "&amp;").replaceAll("\"", "&quot;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("'", "&#39;");
  }
  /**
   * XML schema definition (XSD) fájl generálás.
   * @param jaxbAnnotatedClass Az @XmlRootElement JAXB annotációt tartalmazó gyökér osztály, melyből a generálás történik.
   * @param xsdFile Generálandó XSD fájl.
   * @param printMessage Írjon ki üzenetet a sikeres generálásról ?
   */
  public static void generateXSD(Class jaxbAnnotatedClass, final File xsdFile, boolean printMessage) throws JAXBException, IOException {
    JAXBContext jaxbContext = JAXBContext.newInstance(jaxbAnnotatedClass);
    jaxbContext.generateSchema(
      new SchemaOutputResolver() {
        @Override
        public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException {
          return new StreamResult(xsdFile);
        }
      }
    );
    if (printMessage) {
      System.out.println("XSD GENERÁLÁS: " + jaxbAnnotatedClass.getName() +" osztály alapján az XSD fájl elkészült: " + xsdFile);
    }  
  }
  // =====
}
