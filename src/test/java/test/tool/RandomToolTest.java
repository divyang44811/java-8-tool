package test.tool;
import java.util.Arrays;
import java.util.logging.Logger;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import test.TestInit;
import xesj.tool.RandomTool;
import static xesj.tool.RandomTool.*;
public class RandomToolTest {
  /**
   * Ismétlések száma.
   */
  private static final int REPEAT = 1000;
  /**
   * Logger
   */
  private static final Logger logger = TestInit.getInstance().getLogger(RandomToolTest.class, "random-tool-test-log.txt");
  /**
   * Metódus teszt: interval()
   */
  @Test
  public void intervalTest() {
    int[] cases = {1,3,  -7,-1,  3,3,  0,0,  -53,-53,  -13,5,  -50,50, 
      Integer.MAX_VALUE-5,Integer.MAX_VALUE,  
      Integer.MIN_VALUE,Integer.MIN_VALUE+3,
      Integer.MIN_VALUE,Integer.MAX_VALUE
    };
    int[] dest = new int[200];
    // Ellenőrzés: a tartományba esik-e, és egyenletes-e az eloszlása
    logger.info("\ninterval():");
    logger.info("-----------");
    for (int index = 0; index < cases.length; index += 2) {
      Arrays.fill(dest, 0);
      int min = cases[index], max = cases[index+1];
      boolean eloszlasSzamitas = (min >= -100 && max < 100);
      for (int repeat = 0; repeat < REPEAT; repeat++) {
        int rnd = interval(min, max);
        assertTrue(min <= rnd && rnd <= max);
        if (eloszlasSzamitas) dest[rnd+100]++;
      }  
      if (eloszlasSzamitas) {
        logger.info("Eloszlás: " + min + "..." + max);
        for (int d = min + 100; d <= max + 100; d++) {
          logger.info((d-100) + "=>" + (100.0 * dest[d] / REPEAT) + "%  ");
        }  
        logger.info("");
      }  
    }
  }
  /**
   * Metódus teszt: letters()
   */
  @Test
  public void lettersTest() {    
    logger.info("\nletters(9):");
    logger.info("---------");
    for (int i = 0; i < 5; i++) logger.info(letters(9));
  }
  /**
   * Metódus teszt: digits()
   */
  @Test
  public void digitsTest() {    
    logger.info("\ndigits(22):");
    logger.info("----------------------");
    for (int i = 0; i < 5; i++) logger.info(digits(22));
  }
  /**
   * Metódus teszt: allCharacters()
   */
  @Test
  public void allCharactersTest() {    
    logger.info("\nallCharacters(25):");
    logger.info("-------------------------");
    for (int i = 0; i < 5; i++) logger.info(allCharacters(25));
  }
  /**
   * Metódus teszt: fromCharacters()
   */
  @Test
  public void fromCharactersTest() {    
    logger.info("\nfromCharacters(11):");
    logger.info("-----------");
    for (int i = 0; i < 5; i++) logger.info(fromCharacters("őÍ", 11));
    // length = 0 eset
    assertEquals("", fromCharacters("%/F+", 0));
    assertEquals("", fromCharacters(RandomTool.SPECIAL_CHARACTERS, 0));
  }
  /**
   * Metódus teszt: logic()
   */
  @Test
  public void logicTest() {    
    int logicTrue = 0, logicFalse = 0;
    for (int i = 0; i < REPEAT; i++) {
      if (logic()) logicTrue++;
      else logicFalse++;
    }
    logger.info("\nlogic():");
    logger.info("--------");
    logger.info("Eloszlás: true/false");
    logger.info("true  => " + (100.0 * logicTrue / REPEAT) + "%");
    logger.info("false => " + (100.0 * logicFalse / REPEAT) + "%");
  }
  /**
   * Metódus exception teszt: fromCharacters()
   * A string null.
   */
  @Test(expectedExceptions = RuntimeException.class)
  public void fromCharactersExceptionTest1() {
    fromCharacters(null, 5);
  }
  /**
   * Metódus exception teszt: fromCharacters()
   * A string üres.
   */
  @Test(expectedExceptions = RuntimeException.class)
  public void fromCharactersExceptionTest2() {
    fromCharacters("", 5);
  }
  /**
   * Metódus exception teszt: fromCharacters()
   * A hossz kisebb mint 0.
   */
  @Test(expectedExceptions = RuntimeException.class)
  public void fromCharactersExceptionTest3() {
    fromCharacters("ABC", -1);
  }
  // ===== 
}
