package test.tool;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import static xesj.tool.HtmlTool.*;
public class HtmlToolTest {
  /**
   * Metódus teszt: escape()
   */
  @Test
  public void escapeTest() {
    assertNull(escape(null));
    assertEquals("", escape(""));
    assertEquals("árvíztűrő", escape("árvíztűrő"));
    assertEquals("a &amp; &quot; &lt; &gt; b &#39;", escape("a & \" < > b '"));
  }
  // =====  
}
