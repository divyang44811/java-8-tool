package test.tool;
import java.text.ParseException;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import static xesj.tool.ByteTool.*;

public class ByteToolTest {

  /**
   * Metódus teszt: byteToHex()
   */
  @Test
  public void byteToHexTest() throws ParseException {
    assertNull(byteToHex(null));
    assertEquals("80", byteToHex((byte)-128));
    assertEquals("FE", byteToHex((byte)-2));
    assertEquals("FF", byteToHex((byte)-1));
    assertEquals("00", byteToHex((byte)0));
    assertEquals("01", byteToHex((byte)1));
    assertEquals("0F", byteToHex((byte)15));
    assertEquals("10", byteToHex((byte)16));
    assertEquals("7F", byteToHex((byte)127));
  }

  /**
   * Metódus teszt: hexToByte()
   */
  @Test
  public void hexToByteTest() {    
    // null érték
    assertNull(hexToByte(null));
    // 1 karakteres értékek 
    assertEquals((byte)0, (byte)hexToByte("0"));
    assertEquals((byte)10, (byte)hexToByte("a"));
    assertEquals((byte)15, (byte)hexToByte("f"));
    // 2 karakteres értékek
    assertEquals((byte)-128, (byte)hexToByte("80"));
    assertEquals((byte)-2, (byte)hexToByte("fE"));
    assertEquals((byte)-1, (byte)hexToByte("ff"));
    assertEquals((byte)0, (byte)hexToByte("00"));
    assertEquals((byte)1, (byte)hexToByte("01"));
    assertEquals((byte)15, (byte)hexToByte("0F"));
    assertEquals((byte)16, (byte)hexToByte("10"));
    assertEquals((byte)127, (byte)hexToByte("7f"));
  }

  /**
   * Metódus exception teszt: hexToByte()
   * Hiba: túl hosszú paraméter.
   */
  @Test(expectedExceptions = NumberFormatException.class) 
  public void hexToByteExceptionTest1() {
    hexToByte("300");
  }

  /**
   * Metódus exception teszt: hexToByte()
   * Hiba: túl rövid paraméter.
   */
  @Test(expectedExceptions = NumberFormatException.class) 
  public void hexToByteExceptionTest2() {
    hexToByte("");
  }

  /**
   * Metódus exception teszt: hexToByte()
   * Hiba: nem konvertálható paraméter.
   */
  @Test(expectedExceptions = NumberFormatException.class) 
  public void hexToByteExceptionTest3() {
    hexToByte("x0");
  }

  /**
   * Metódus teszt: byteToBin()
   */
  @Test
  public void byteToBinTest() {    
    assertNull(byteToBin(null));
    assertEquals("10000000", byteToBin((byte)-128));
    assertEquals("11111110", byteToBin((byte)-2));
    assertEquals("11111111", byteToBin((byte)-1));
    assertEquals("00000000", byteToBin((byte)0));
    assertEquals("00000001", byteToBin((byte)1));
    assertEquals("00000010", byteToBin((byte)2));
    assertEquals("00000111", byteToBin((byte)7));
    assertEquals("01111111", byteToBin((byte)127));
  }

  /**
   * Metódus teszt: binToByte()
   */
  @Test
  public void binToByteTest() {    
    assertNull(binToByte(null));
    assertEquals((byte)-128, (byte)binToByte("10000000"));
    assertEquals((byte)-2,   (byte)binToByte("11111110"));
    assertEquals((byte)-1,   (byte)binToByte("11111111"));
    assertEquals((byte)0,    (byte)binToByte(       "0"));
    assertEquals((byte)0,    (byte)binToByte("00000000"));
    assertEquals((byte)1,    (byte)binToByte(       "1"));
    assertEquals((byte)1,    (byte)binToByte(     "001"));
    assertEquals((byte)7,    (byte)binToByte("00000111"));
    assertEquals((byte)8,    (byte)binToByte(    "1000"));
    assertEquals((byte)127,  (byte)binToByte("01111111"));
    assertEquals((byte)127,  (byte)binToByte( "1111111"));
  }

  /**
   * Metódus exception teszt: binToByte()
   * Hiba: nem konvertálható paraméter.
   */
  @Test(expectedExceptions = NumberFormatException.class) 
  public void binToByteExceptionTest1() {
    binToByte("0x1111");
  }

  /**
   * Metódus exception teszt: binToByte()
   * Hiba: túl hosszú paraméter.
   */
  @Test(expectedExceptions = NumberFormatException.class) 
  public void binToByteExceptionTest2() {
    binToByte("100000000");
  }

  /**
   * Metódus exception teszt: binToByte()
   * Hiba: túl rövid paraméter.
   */
  @Test(expectedExceptions = NumberFormatException.class) 
  public void binToByteExceptionTest3() {
    binToByte("");
  }

  // =====
}
