package test.tool;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import xesj.tool.InitSample;
public class InitSampleTest {
  /**
   * Metódusok tesztelése.
   */
  @Test
  public void test() {
    System.setProperty("kornyezet", "dev");
    InitSample init = InitSample.getInstance();
    int rnd = init.getRandom();
    assertEquals(rnd, InitSample.getInstance().getRandom());
    assertEquals(rnd, InitSample.getInstance().getRandom());
    assertEquals(rnd, InitSample.getInstance().getRandom());
    // reinit
    InitSample.reInit();
    init = InitSample.getInstance();
    int rnd2 = init.getRandom();
    assertNotSame(rnd, rnd2);
    assertEquals(rnd2, InitSample.getInstance().getRandom());
    assertEquals(rnd2, InitSample.getInstance().getRandom());
    assertEquals(rnd2, InitSample.getInstance().getRandom());
  }
  // =====
}
