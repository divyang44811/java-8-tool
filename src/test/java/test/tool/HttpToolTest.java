package test.tool;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import static xesj.tool.HttpTool.*;
public class HttpToolTest {
  /**
   * Metódus teszt: encodeURL()
   */
  @Test
  public void encodeURLTest() {
    assertNull(null);
    assertEquals("%2F", encodeURL("/"));
    assertEquals("%C3%A1rv%C3%ADzt%C5%B1r%C5%91", encodeURL("árvíztűrő"));
  }
  // =====
}
