package test.tool;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Logger;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import test.TestInit;
import static xesj.tool.FileTool.*;
import xesj.tool.StreamTool;
public class FileToolTest {
  /**
   * Logger
   */
  private static final Logger logger = TestInit.getInstance().getLogger(FileToolTest.class, "file-tool-test-log.txt");
  /**
   * Metódus teszt: tmpDir(), tmpDirName()
   */
  @Test
  public void tmpDirTest() throws IOException, URISyntaxException {
    logger.info("tmpDirName() -> " + tmpDirName());
    logger.info("tmpDir()     -> " + tmpDir());
  }
  /**
   * Metódus teszt: tmpFile()
   */
  @Test
  public void tmpFileTest() {
    for (int i = 0; i < 3; i++) {
      File tmpFile = tmpFile(null);
      logger.info("Temporális fájl kiterjesztés nélkül: " + tmpFile.getAbsolutePath());
    }
    for (int i = 0; i < 3; i++) {
      File tmpFile = tmpFile("jpg");
      logger.info("Temporális fájl kiterjesztéssel:     " + tmpFile.getAbsolutePath());
    }
  }
  /**
   * Metódus teszt: contentEquals()
   */
  @Test
  public void contentEqualsTest() throws IOException, URISyntaxException {
    File 
      file1 = new File(this.getClass().getResource("adatmodell.jpg").toURI()),
      file2 = new File(this.getClass().getResource("auto.jpg").toURI()),
      file2m = new File(this.getClass().getResource("auto_masolat.jpg").toURI()),
      fileUres = new File(this.getClass().getResource("ures.xy").toURI()),
      dir = tmpDir();
    // false esetek
    assertFalse(contentEquals(file1, file2));
    assertFalse(contentEquals(fileUres, file1));
    assertFalse(contentEquals(file1, fileUres));
    // true esetek
    assertTrue(contentEquals(file2m, file2));
    assertTrue(contentEquals(file2, file2m));
    assertTrue(contentEquals(file2, file2));
    assertTrue(contentEquals(fileUres, fileUres));
  }
  /**
   * Metódus exception teszt: contentEquals()
   * Hiba: a paraméter null.
   */
  @Test(expectedExceptions = NullPointerException.class)
  public void contentEqualsExceptionTest1() throws IOException  {
    contentEquals(null, null);
  }
  /**
   * Metódus exception teszt: contentEquals()
   * Hiba: a paraméter könyvtár.
   */
  @Test(expectedExceptions = FileNotFoundException.class)
  public void contentEqualsExceptionTest2() throws IOException {
    contentEquals(tmpDir(), tmpDir());
  }
  /**
   * Metódus exception teszt: contentEquals()
   * Hiba: az egyik paraméter nemlétező fájl.
   */
  @Test(expectedExceptions = FileNotFoundException.class)
  public void contentEqualsExceptionTest3() throws IOException {
    File 
      letezo = tmpFile("txt"),
      nemLetezo = tmpFile("txt");
    try (FileOutputStream fileOutputStream = new FileOutputStream(letezo)) {
      StreamTool.writeString("abc", fileOutputStream, "UTF-8");
    }
    contentEquals(letezo, nemLetezo);
  }
  // =====
}
