package test.tool;
import java.util.Date;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import xesj.tool.SQLTool;

public class SQLToolTest {

  /**
   * Konstruktor teszt
   */
  @Test
  public void constructorTest() {
    SQLTool st;
    
    // Üres konstruktor
    st = new SQLTool();
    assertEquals("", st.toString());
    
    // Paraméteres konstruktor
    st = new SQLTool(null);
    assertEquals("", st.toString());

    st = new SQLTool("");
    assertEquals("", st.toString());

    st = new SQLTool(" ");
    assertEquals(" ", st.toString());
  }

  /**
   * add() metódus teszt
   */
  @Test
  public void addTest() {
    SQLTool st;

    st = new SQLTool().add(null);
    assertEquals("", st.toString());

    st = new SQLTool("abc").add(null);
    assertEquals("abc", st.toString());

    st = new SQLTool("abc").add(" DEF ");
    assertEquals("abc DEF ", st.toString());
  }

  /**
   * itr() metódus teszt
   */
  @Test
  public void addIfTest() {
    SQLTool st;

    st = new SQLTool().itr(null, true);
    assertEquals("", st.toString());

    st = new SQLTool("abc").itr("XY", false);
    assertEquals("abc", st.toString());

    st = new SQLTool("abc").itr(" DEF ", true);
    assertEquals("abc DEF ", st.toString());
  }
  
  /**
   * inn() metódus teszt
   */
  @Test
  public void addIfNotNullTest() {
    SQLTool st;

    st = new SQLTool().inn("CVB", null);
    assertEquals("", st.toString());

    st = new SQLTool("abc").inn("XY", "");
    assertEquals("abcXY", st.toString());

    st = new SQLTool("abc").inn(" DEF ", null);
    assertEquals("abc", st.toString());
  }
  
  /**
   * Vegyes teszt
   */
  @Test
  public void miscTest() {
    SQLTool st;

    st = new SQLTool().add("H").inn("CVB", null).inn("avasi", ".").itr(" gyopár", true).itr("!", false);
    assertEquals("Havasi gyopár", st.toString());

    st = new SQLTool("Bar").add("na").itr("med", 1 < 2).inn("ve", new Date()).itr(null, true).itr("", !false);
    assertEquals("Barnamedve", st.toString());
  }
  
  // =====  
}
