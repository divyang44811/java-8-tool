package test.shell.other;
import test.shell.other.Elso;
import java.sql.ResultSet;
import java.sql.SQLException;
import xesj.shell.ResultSetConverter;
public class ElsoResultSetConverter implements ResultSetConverter<Elso>{
  @Override
  public Elso convert(ResultSet resultSet, int rowNumber) {
    Elso elso = new Elso();
    try {
      elso.id = resultSet.getLong("id");
      elso.rovid_nev = resultSet.getString("rovid_nev");
      elso.hosszu_nev = "ROW-NUMBER: " + rowNumber + " " + resultSet.getString("hosszu_nev");
    } 
    catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return elso;
  }
}
